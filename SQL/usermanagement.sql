﻿CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;
USE usermanagement;

CREATE TABLE user( 
    id SERIAL,
    login_id varchar (255) UNIQUE NOT NULL,
    name varchar (255) NOT NULL,
    birth_date DATE NOT NULL,
    password varchar (255) NOT NULL,
    create_date DATETIME NOT NULL,
    update_date DATETIME NOT NULL
);

insert into user value (1, 'admin', '管理者','1998-01-01','0001','2021-03-03',NOW());


