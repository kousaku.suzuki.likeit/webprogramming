package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class Update
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		 User user =(User)session.getAttribute("userInfo");

		    if (user == null){

		      response.sendRedirect("LoginServlet");
		      return;
		    }

		String id = request.getParameter("id");

		UserDao userDao = new UserDao();
		User user2 = userDao.findByDetail(id);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("user", user2);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String password = request.getParameter("password");
		String confirmation = request.getParameter("confirmation");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String loginId = request.getParameter("loginId");

		//UserDaoにデータ追加のメソッドを作る
		UserDao userDao = new UserDao();



		if (name.equals("")||birthDate.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if(!password.equals(confirmation)){
        	request.setAttribute("errMsg", "入力された内容は正しくありません");
        	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response);
			return;
        }

		if(password.equals("")&&confirmation.equals("")) {
			userDao.updateUser(name, birthDate, loginId);

			response.sendRedirect("ListServlet");
			return;
		}

		userDao.updateAccount(password, name, birthDate, loginId);

		response.sendRedirect("ListServlet");
	}
}
