<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ログイン画面</title>

	<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	 integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	 crossorigin="anonymous">

</head>
	<body>
        <h2 style="text-align:center">ログイン画面</h2>
        <c:if test="${errMsg != null}" >
	   		<div class="alert alert-danger" role="alert">
		 	 ${errMsg}
			</div>
		</c:if>
		<form style="text-align:center" class="form-signin" action="LoginServlet" method="post">
            <div class="form-group row col-4 mx-auto" >
                <label for="inputId">ログインID</label>
                <input type="text" name="loginId" class="form-control" id="inputId" style="width:200px;" >
            </div>
            <div class="form-group row col-4 mx-auto">
                <label for="InputPassword">パスワード</label>
                <input type="password" name="password" class="form-control" id="InputPassword" style="width:200px;">
            </div>
            <button type="submit" class="btn btn-primary">ログイン</button>
        </form>
	</body>
</html>