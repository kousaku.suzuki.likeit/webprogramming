<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザー情報詳細参照</title>

	<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	 integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	 crossorigin="anonymous">

</head>
	<body>

        <a href ="Logout">ログアウト</a>
        <h2 style="text-align:center">ユーザー情報詳細参照</h2>
		<br>
        <div class="container">
  <div class="row">
    <div class="col">ログインID</div>
    <div class="col">${user.loginId}</div>
    <div class="w-100"></div>
    <div class="col">ユーザー名</div>
    <div class="col">${user.name}</div>
    <div class="w-100"></div>
    <div class="col">生年月日</div>
    <div class="col">${user.birthDate}</div>
    <div class="w-100"></div>
    <div class="col">登録日時</div>
    <div class="col">${user.createDate}</div>
    <div class="w-100"></div>
    <div class="col">更新日時</div>
    <div class="col">${user.updateDate}</div>
  </div>
</div>
        <a href ="ListServlet">戻る</a>
	</body>
</html>