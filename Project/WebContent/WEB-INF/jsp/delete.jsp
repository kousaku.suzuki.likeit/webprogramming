<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザー削除確認</title>

	<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	 integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	 crossorigin="anonymous">

</head>
	<body>
        <a href="Logout">ログアウト</a>
        <h2 style="text-align:center">ユーザー削除確認</h2>
        <br>
        <p>ログインID:${user.loginId}</p>
        <p>を本当に削除してよろしいでしょうか。</p>
        <br>
		<a href ="ListServlet" type="button" class="btn btn-secondary btn-lg">キャンセル</a>
		<form action="DeleteServlet" method="post">
			<input type="hidden" value="${user.id}" name="id">
        	<button type="submit" class="btn btn-primary btn-lg">OK</button>
        </form>
	</body>
</html>