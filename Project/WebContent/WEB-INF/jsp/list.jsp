<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザー一覧画面</title>

	<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	 integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	 crossorigin="anonymous">

</head>
	<body>
		<header>
			<nav class="navbar navbar-inverse">
      			<div class="container">
         		<ul class="nav navbar-nav navbar-right">
           			<li class="navbar-text">${userInfo.name} さん </li>
					<li class="dropdown">
  			 			<a href="Logout" class="navbar-link logout-link">ログアウト</a>
            		</li>
  		  		</ul>
      			</div>
     		</nav>
   		</header>
       	<br>
        <h2 style="text-align:center">ユーザー一覧画面</h2>
        <br>
        <a href="RegisterServlet">新規登録</a>
        <br>
		<form action="ListServlet" method="post">
            <div class="form-group row col-4 mx-auto" >
                <label for="inputId">ログインID</label>
                <input type="text" name="loginId" class="form-control" id="inputId" style="width:200px;" >
            </div>

            <div class="form-group row col-4 mx-auto">
                <label for="InputNama">ユーザー名</label>
                <input type="text" name="name" class="form-control" id="InputName" style="width:200px;">
            </div>

            <div class="form-group">
                  <label for="continent" class="control-label col-sm-2">生年月日</label>
                  <div class="row">
                    <div class="col-sm-2">
                      <input type="date" name="date-start" id="date-start" class="form-control" size="30"/>
                    </div>
                    <div class="col-xs-1 text-center">
                      ~
                    </div>
                    <div class="col-sm-2">
                      <input type="date" name="date-end" id="date-end" class="form-control"/>
                    </div>
                </div>
             </div>

            <div class="row col-6">
                <button type="submit" class="btn btn-primary">検索</button>
            </div>
        </form>

        <br>

        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">ログインID</th>
                    <th scope="col">ユーザー名</th>
                    <th scope="col">生年月日</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                 <c:forEach var="user" items="${userList}" >
                   <tr>

                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>

                     <!-- TODO 未実装；ログインボタンの表示制御を行う -->
                     <c:if test="${userInfo.id == 1}">
                     <td>
                       <a class="btn btn-primary" href="DetailServlet?id=${user.id}">詳細</a>
                       <a class="btn btn-success" href="UpdateServlet?id=${user.id}">更新</a>
                       <a class="btn btn-danger" href ="DeleteServlet?id=${user.id}">削除</a>
                     </td>
                     </c:if>

                     <c:if test="${userInfo.id != 1}">
                     <td>
                       <a class="btn btn-primary" href="DetailServlet?id=${user.id}">詳細</a>
                      <c:if test="${userInfo.id == user.id}">
                      <a class="btn btn-success" href="UpdateServlet?id=${user.id}">更新</a>
                      </c:if>
                     </td>
                     </c:if>

                   </tr>
                 </c:forEach>
            </tbody>
        </table>
	</body>
</html>