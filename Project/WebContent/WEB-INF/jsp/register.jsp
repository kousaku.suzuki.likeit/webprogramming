<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザー新規登録</title>

	<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	 integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	 crossorigin="anonymous">

</head>
	<body>
        <a href="Logout">ログアウト</a>
        <h2 style="text-align:center">ユーザー新規登録</h2>
        <c:if test="${errMsg != null}" >
	   		<div class="alert alert-danger" role="alert">
		 	 ${errMsg}
			</div>
		</c:if>
        <br>
		<form style="text-align:center" class="form-signin" action="RegisterServlet" method="post">
            <div class="form-group row col-4 mx-auto" >
                <label for="inputId">ログインID</label>
                <input type="text" name="loginId" class="form-control" id="inputId" style="width:200px;" >
            </div>

            <div class="form-group row col-4 mx-auto">
                <label for="InputPassword">パスワード</label>
                <input type="password" name="password" class="form-control" id="InputPassword" style="width:200px;">
            </div>

            <div class="form-group row col-4 mx-auto">
                <label for="InputPassword">パスワード（確認）</label>
                <input type="password" name="confirmation" class="form-control" id="InputPassword" style="width:200px;">
            </div>

             <div class="form-group row col-4 mx-auto">
                <label for="InputPassword">ユーザー名</label>
                <input type="text" name="name" class="form-control" id="InputPassword" style="width:200px;">
            </div>

             <div class="form-group row col-4 mx-auto">
                <label for="InputPassword">生年月日</label>
                <input type="text" name="birthDate" class="form-control" id="InputPassword" style="width:200px;">
            </div>

            <button type="submit" class="btn btn-primary">登録</button>
        </form>
        <a href="ListServlet">戻る</a>
	</body>
</html>